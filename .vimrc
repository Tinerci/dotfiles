
set number relativenumber "linenumbers
set incsearch "search while typing
set hlsearch "highligh search reseults

"Plugins

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-commentary' "comment gcc & uncomment gcgc
Plug 'dracula/vim' "colorscheme
Plug 'preservim/nerdtree' "filetree
call plug#end()



"enable dracula colorscheme
syntax enable
colorscheme dracula
set background=dark

