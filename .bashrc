#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi

export EDITOR=vim
export VISUAL=vim
export PS1="\[\033[38;5;6m\]\h@\u\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;5m\][\w]\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;5m\]\\$:\[$(tput sgr0)\] \[$(tput sgr0)\]"
export LESSHISTFILE=-

[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

